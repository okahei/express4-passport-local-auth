const appConf = require(__dirname + '/../../conf/app.json');
const models = require(__dirname + '/../models');
const colors = require('colors/safe');

function create_first_user(user) {
    return new Promise(function (resolve, reject) {
        models.User.findOne({
            where:{
                username: user.username
            },
            include:[{
                model: models.Role,
                where:{
                    role: appConf.users.roles.admin
                }
            }]
        }).then(function(luser){
            if (!luser) return create();
            console.log(colors.cyan("Admin Username: " + user.username + " exists in DB"));
            return resolve(luser);
        });

        function create(){
            models.Role.findOne({
                where: {
                    role: appConf.users.roles.admin
                }
            }).then(function(role){
                if(!role) throw new Error('create_first_user Error: no admin Role found');
                models.User.create({
                    username: user.username,
                    email: user.email,
                    password: user.password,
                    roleId: role.id
                }).then(function (luser) {
                    console.log(colors.cyan('New User Created: '));
                    console.log(colors.cyan("Username, Password: ", user.username, user.password));
                    return resolve(luser)
                }, function (e) {
                    throw e
                });
            }, function (e) {
                throw e
            });
        }
    })
}

module.exports = create_first_user;
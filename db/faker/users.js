var faker = require('faker');
var bcrypt = require('bcrypt-nodejs');
var password = '1234';
var moment = require('moment');
var _ = require('underscore');
var appconf = require(__dirname + '/../../conf/app.json');
const role_ids = Object.keys(appconf.users.roles);

class FakeUser {
    constructor (luser){
        this.attributes = {
            username: luser ? luser.username : faker.internet.userName(),
            email: faker.internet.email(),
            password: bcrypt.hashSync(password),
            password_expiration: moment(new Date()).utc().add(1, 'month').format("YYYY-MM-DD HH:mm:ss"),
            createdTimestamp: moment(new Date()).utc().format("YYYY-MM-DD HH:mm:ss"),
            updateTimestamp: moment(new Date()).utc().format("YYYY-MM-DD HH:mm:ss"),
            roleId: luser ? luser.roleId : faker.random.arrayElement(_.range(1, (role_ids.length + 1)))
        }
    }
    
    get(){
        return this.attributes;
    }
}

/**
 * 2017-02-06T17:51:57.000Z
 * Generate fake user
 *
 * @param n number of users to generate
 * @returns {Array}
 */
function generateUsers(n) {
    var c = [];
    for (var i = 0; i <= n - 1; i++) {
        var user = new FakeUser();
        c.push(user.get());
    }
    return c;
}

/**
 * generate Given User
 * 
 * username and roleId
 * 
 * @param luser
 * @returns {*}
 */
function generateGivenUser(luser){
    let user = new FakeUser(luser);
    return user.get()
}


module.exports = {
    generateGivenUser:generateGivenUser,
    generateUsers: generateUsers 
};
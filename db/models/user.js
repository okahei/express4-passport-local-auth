'use strict';
//models.User.findOne({ where:{username: "pepe"}, include:[models.Role]}).then(function(luser){ console.log(luser.toJSON()) })
const bcrypt = require('bcrypt-nodejs');
const moment = require('moment');
const _ = require('underscore');
const colors = require('colors');

module.exports = function (sequelize, DataTypes) {
    var User = sequelize.define('User', {
        username: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
        banned: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        password: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
        password_expiration: {
            type: DataTypes.DATE,
            allowNull: true
        },
        resetPasswordToken: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: true
        },
        resetPasswordExpires: {
            type: DataTypes.DATE,
            allowNull: true
        }
    }, {
        timestamps: true,
        createdAt: 'createdTimestamp',
        updatedAt: 'updateTimestamp',
        deletedAt: 'destroyTime',
        paranoid: true,
        classMethods: {
            associate: function (models) {
                User.belongsTo(models.Role, {foreignKey: 'roleId'});
            }
        },
        instanceMethods:{
            toJSON: function () {
                return _.omit(this.get({plain:true}), 'password');
            },
            role: function () {
                return this.get('Role').role;
            }
        }
    });

    
    User.beforeCreate(function (user, options) {
        user.password = bcrypt.hashSync(user.password);
        user.password_expiration = moment(new Date()).add(1, 'month');
    });

    User.beforeUpdate(function (user, options) {
        if (user.changed('password') && user.password.length > 3) {
            user.password = bcrypt.hashSync(user.password);
            user.password_expiration = moment(new Date()).add(1, 'month');
        }
    });

    User.beforeBulkCreate(function (users, options) {
        users.forEach(function(user){
            user.password = bcrypt.hashSync(user.password);
            console.log(user);
        })
    });
    

    return User;
};
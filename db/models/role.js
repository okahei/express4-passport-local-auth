'use strict';

module.exports = function(sequelize, DataTypes) {
    var Role = sequelize.define('Role', {
        role: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        }
    }, {
        timestamps: false,
        classMethods: {
            associate: function(models) {
                //Role.hasOne(models.User);
            }
        }
    });

    return Role;
};
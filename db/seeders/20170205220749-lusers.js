/**
 * User Seed File
 */

'use strict';
const appConf = require(__dirname + '/../../conf/app.json');
const fakeLusers = require(__dirname + '/../faker/users').generateUsers;
const generateGivenUser = require(__dirname + '/../faker/users').generateGivenUser;
const create_first_admin_user = require(__dirname + '/../faker/createAdminUser');

module.exports = {
    up: function (queryInterface, Sequelize) {
        return create_first_admin_user(appConf.users.firstAdmin)
            .then(() => {

                const faked = [];
                faked.push(generateGivenUser({username: 'FooUser', roleId: 1}));
                faked.push(generateGivenUser({username: 'FooManager', roleId: 2}));
                let fakedUsers = fakeLusers(50);

                return queryInterface
                    .bulkInsert('Users', fakedUsers.concat(faked));
            }, null);
    },

    down: function (queryInterface, Sequelize) {
        return queryInterface.bulkDelete('Users', null, {});
    }
};

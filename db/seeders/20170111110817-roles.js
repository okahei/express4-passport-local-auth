'use strict';

const appConf = require(__dirname + '/../../conf/app.json');

module.exports = {
    up: function (queryInterface, Sequelize) {

        const userRoles = Object.keys(appConf.users.roles)
            .map(role => {
                return {role: appConf.users.roles[role]}
            });

        //return queryInterface.bulkInsert('Roles', userRoles);
        return queryInterface.bulkInsert('Roles', userRoles)
            .then(roles => roles)
            .catch(e => {
                // if app has been started before running any test there should be roles
                if(e.errors[0].message == 'role must be unique') return null;
                throw new Error(e)
            })
    },

    down: function (queryInterface, Sequelize) {
        return queryInterface.bulkDelete('Roles', null, {});
    }
};

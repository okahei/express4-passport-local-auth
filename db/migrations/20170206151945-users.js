'use strict';

var models = require(__dirname + "/../models");

module.exports = {
    up: function (queryInterface, Sequelize) {
        return queryInterface.createTable(models.User.tableName,
            models.User.attributes);
    },
    down: function (queryInterface, Sequelize) {
        return queryInterface.dropTable("Users");
    }
};

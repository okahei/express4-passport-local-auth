'use strict';

var models = require(__dirname + "/../models");

module.exports = {
  up: function (queryInterface, Sequelize) {
      return queryInterface.createTable(models.Role.tableName,
          models.Role.attributes);
  },

  down: function (queryInterface, Sequelize) {
      return queryInterface.dropTable("Roles");
  }
};

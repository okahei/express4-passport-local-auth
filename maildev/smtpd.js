var appConf = require(__dirname + '/../conf/app.json');

var MailDev = require('maildev');

var maildev = new MailDev({
    smtp: appConf.development.mailer.transport.port,
    web: appConf.development.maildev.web,
    silent: true
});


module.exports = {
    start: maildev.listen,
    on: maildev.on
};

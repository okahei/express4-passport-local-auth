/**
 *  EXEC NODEJS NOBODY/NOGROUP iif app started by Root
 *
 */
if (process.getgid() === 0) {
    process.setgid('nogroup');
    process.setuid('nobody');
    console.log("Executing app with GID:" + process.getgid());
    console.log("Executing app with UID:" +process.getuid());
}
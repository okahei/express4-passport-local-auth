"use strict";

const fs = require('fs');

const options = {
    key: fs.readFileSync(__dirname + '/../ssl/key.pem'),
    cert: fs.readFileSync(__dirname + '/../ssl/cert.pem')
};

module.exports = options;
const routes = {
    home: {
        mount: "/",
        controller: null,
        subroutes: [
            {
                name: "home",
                url: ""
            },
            {
                name: "home.logout",
                url: "logout"
            }
        ]
    },
    auth: {
        mount: "/auth",
        controller: null,
        subroutes: [
            {
                name: "auth.login",
                url: "/login"
            },
            {
                name: "auth.forgot",
                url: "/forgot"
            },
            {
                name: "auth.reset",
                url: "/reset/:token"
            }
        ]
    },
    users:{
        mount: "/users",
        controller: null,
        subroutes: [
            {
                name: "users.changepassword",
                url: "/changepassword"
            },
            {
                name: "users.profile",
                url: "/profile"
            }
        ]
    },
    admin:{
        mount: "/admin",
        constroller: null,
        subroutes: [
            {
                name: "admin.users",
                url: "/users/list/:page?"
            },
            {
                name: "admin.users.profile",
                url: "/users/:userId" //url valid for GET PATCH DELETE
            },
            {
                name: "admin.users.create",
                url: "/users/create"
            },
            {
                name: "admin.users.delete",
                url: "/users/delete"
            }
        ]
    }
};

module.exports = routes;
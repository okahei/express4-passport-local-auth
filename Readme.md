#Express 4 Auth Passport Local

Example Express 4 app Passport Auth Local users against mysql with
Cookies and Session (stored in a Redis database) and recover password mode.

Two mysql tables, Users and Roles with Sequelize ORM.

* On first Start, Express
    * will populate roles table from users.roles in /conf/app.json
    * will create admin user with data from firstAdminUser in /conf/app.json

#### Prerequisites

* Mysql Server
* Redis Server
* nodejs >= 6.X

##### App Mode
App **development/production mode** must be configured in /conf/app.json

##### Auth and Cookie Domain
By default it will only run in localhost, so if you want auth works with a different domain
you should change **host** entry in /conf/auth.json

##### SSL
If started in Production Mode Express will setup Https Server (ssl), otherwise it will setup normal Http server


### Download and Install:
```sh
$ git clone git@bitbucket.org:okahei/express-passport.git
$ cd express4-passport-local-auth/
$ npm install
```

### Folder Perms
```sh
$ sudo chown -R nobody:`whoami` maildev/
$ sudo chown -R nobody:`whoami` logs/
```

## Mysql
Edit db/config/config.json and set database data.

```sh
$ mysql -uroot -p1 -e "create database express_auth_dev;"
$ mysql -uroot -p1 -e "create database express_auth_prod;"
```

#### Create tables
```sh
$ npm run init-db

############
# production mode?
$ export NODE_ENV=production; npm run init-db
```

### Start App

```sh
$ npm start
```

### Tests

In development mode (default), Espress App will start Maildev App with the given options in /conf/app.json

* Successfully User Login
* Wrong User/password Login
* Recover password
    * post forgot password
    * get email with token from maildev
    * post token and change password
* Login as Admin
* Patch User Profile

#### Seed Users/roles for test
```sh
$ npm run seed-db
```

####Run tests

```sh
npm test
```

##FrontEnd

##### Grunt + Browserify + Browserify-shim

```sh
$ grunt -v
```

* clean build directories
* copy assets (fonts)
* cssmin .Css files
* browserify .Js files exporting Jquery
* versioning .Js and .Css Files into assets.config.json

Any change made to .Css or .Js files must be followed by grunt command
/*
 *   Execute:
 *    $ grunt -v
 *
 *   Output Files:
 *      Config:
 *          > public/config/assets.config.json
 *      Css:
 *          > public/stylesheets/build/output.e55c3875.min.css
 *      JS:
 *          > public/javascripts/build/build.e44c33dda.min.js
 *
 *   Express Template EJS:
 *    app.locals._assets = require('./public/config/assets.config.json');
 *        Template:
 *            <link rel='stylesheet' href="<%= _assets.staticAssets.global.css %>"/>
 *            <script type="text/javascript" src="<%= _assets.staticAssets.global.js %>" defer></script>
 */
module.exports = function (grunt) {
    const jsBuildPath = 'public/javascripts/build';
    const cssBuildPath = 'public/stylesheets/build';
    
    grunt.initConfig({
        browserify: {
            main: {
                files: [{
                    src: ['public/javascripts/index.js'],
                    dest: jsBuildPath + '/build.js'
                    // Note: The entire `browserify-shim` config is inside `package.json`.
                }]
            }
        },
        cssmin: {
            main: {
                files: [{
                    src: [
                        './node_modules/bootstrap/dist/css/bootstrap.css',
                        './node_modules/font-awesome/css/font-awesome.css',
                        './node_modules/select2/dist/css/select2.css',
                        './public/stylesheets/style.css'
                    ],
                    dest: cssBuildPath + '/output.min.css'
                }]
            }
        },
        versioning: {
            options: {
                cwd: 'public',
                outputConfigDir: 'public/config'
            },
            dist: {
                files: [{
                    assets: '<%= cssmin.main.files %>',
                    key: 'global',
                    dest: "stylesheets/build",
                    type: 'css',
                    ext: '.min.css'
                }, {
                    assets: '<%= browserify.main.files %>',
                    key: 'global',
                    dest: "javascripts/build",
                    type: 'js',
                    ext: '.min.js'
                }]
            }
        },
        copy: {
            faw: {
                expand: true,
                cwd: './node_modules/font-awesome/fonts/',
                src: '**',
                dest: './public/stylesheets/fonts/'
            },
            glyphi:{
                expand: true,
                cwd: './node_modules/bootstrap/fonts/',
                src: '**',
                dest: './public/stylesheets/fonts/'
            }
        },
        clean: [jsBuildPath, cssBuildPath]
    });
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-browserify');
    grunt.loadNpmTasks('grunt-static-versioning');
    grunt.loadNpmTasks('grunt-contrib-copy');
    // Default task(s).
    grunt.registerTask('default', ['clean','copy','cssmin', 'browserify', 'versioning']);

};
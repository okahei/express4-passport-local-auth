const _ = require('underscore');
const Table = require('cli-table');

let table = new Table({
    head: ['mount','path','method'],
    style : {compact : true, 'padding-left' : 1}
});

function listRoutes(ro, mount) {
    if(!ro.controller) return;
    const roclone = Object.assign({}, ro);
    return roclone.controller.stack.filter(function(r){
        if(r.route){
            if(r.route.stack) {
                return r.route.stack[0].name != "middleware" && r.route.stack[0].name != "isLoggedIns";
            }
        }
    }).map(function (r) {
        if(roclone.mount === '/') roclone.mount = '';
        return {
            mount: mount,
            url: roclone.mount + r.route.path,
            method: Object.keys(r.route.methods)[0].toUpperCase()
        }
    })

}

function tableRoutes(routes) {
    Object.keys(routes).forEach(function (route) {
        let group = listRoutes(routes[route], route);
        _.each(group, function (r) {
            table.push(Object.keys(r).map(( k => r[k] )))
        });
    });

    return table.toString();
}

module.exports = tableRoutes
"use strict";

/* pathToRegexp  express reverse routes */
const pathToRegexp = require('path-to-regexp');

/* express app routes */
const routes = require('../conf/routes');

/**
 * Attach build functions
 *  to every master route object
 */
Object.keys(routes).forEach(function(route){
    routes[route].build = build;
    routes[route].buildRawUrl = buildRawUrl;
});

/**
 * Build URL
 *
 * @param name
 * @param params
 * @returns {*}
 */
function build(name, params){
    params = params || {};

    const route = this
        .subroutes
        .filter(r => r.name == name);

    if(route.length !=1 ) throw new Error('build route search route filter');

    const url = this.mount + route[0].url;

    return pathToRegexp.compile(url)(params);
}

/**
 * build express url
 *
 * @param name
 * @returns {*}
 */
function buildRawUrl (name){
    const route = this
        .subroutes
        .filter(r => r.name == name);

    return route[0].url;
}

/**
 * Url helper
 *  very basic helper to build express 4 urls in router module mode
 *  https://github.com/expressjs/express/issues/2768#issuecomment-143911836
 *
 * @param name string
 * @param params object
 * @returns {*}
 */
function url (name, params){
    const mounted = name.split('.');

    if(!routes.hasOwnProperty(mounted[0])) throw new Error('Unknown  << ' + mounted[0] + ' >> route mount url helper');

    if(mounted.length == 1) return routes[mounted[0]].mount;
    return routes[mounted[0]].build(name, params);
}

/**
 * RawUrl
 *
 * used by router to get express routes,
 * ex: users.id => router.get('/:id', ...)
 *     reset.token => router.get('/reset/:token')
 *
 * @param name string
 * @returns {*}
 */
function rawUrl (name){
    const mounted = name.split('.');

    if(mounted.length == 1) return routes[mounted[0]].mount;
    return routes[mounted[0]].buildRawUrl(name);
}

module.exports = {
    url:url,
    rawUrl: rawUrl
};
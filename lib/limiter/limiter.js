/* Login Rate Limiter */

const express = require('express');
const router = express.Router();
const Redisclient = require('redis').createClient();
const limiter = require('express-limiter')(router, Redisclient);

limiter({
        path: '/auth/login',
        method: 'post',
        lookup: ['connection.remoteAddress', 'body.username'],
        total: 150,
        expire: 1000 * 60 * 60, // 150 request in 1 Hour
        onRateLimited: function (req, res, next) {
            console.log('Login Rate limit exceeded');
            log.warn('Login Rate limit exceeded, IP: ' + req.ip + ' Username: ' + req.body.username);
            next({message: 'Login Rate limit exceeded', status: 429})
        }
    }
);
var express = require('express');
var router = express.Router();
var middleware = require('./middleware.js');
const url = require('../lib/urlHelper').url;

/* MIDDLEWARE AUTH*/
router.get('/', middleware.isLoggedIns);

/* GET PANEL */
router.get(url('home'), renderHome);

/* LOGOUT */
router.get(url('home.logout'), logOut);


function renderHome(req, res, next) {
    res.render('home', {
        title: 'Panel',
        user:req.user
    });
}

function logOut(req, res) {
    req.logout();
    res.redirect(url('home'));
}

module.exports = router;
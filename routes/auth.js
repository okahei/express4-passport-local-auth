/* Express Router */
const express = require('express');
const router = express.Router();

/* Csrf */
const csrf = require('../controllers/csrf');

/* Auth controller */
const authCtrl = require('../controllers/auth');

/* url.helper */
const url = require('../lib/urlHelper').rawUrl;

function prot(args){
    console.log(args)
}
/* Login */
router.get(url("auth.login"), csrf.csrfProtection, authCtrl.loginGet);
router.post(url("auth.login"), csrf.parseForm, csrf.csrfProtection, authCtrl.passportAuth);

/* Forgot */
router.get(url("auth.forgot"), csrf.csrfProtection, authCtrl.forgotGet);
router.post(url("auth.forgot"), csrf.parseForm, csrf.csrfProtection, authCtrl.forgotPost);

/* Reset */
router.get(url("auth.reset"), csrf.csrfProtection, authCtrl.resetGet);
router.post(url("auth.reset"), csrf.parseForm, csrf.csrfProtection, authCtrl.resetPost);

module.exports = router;
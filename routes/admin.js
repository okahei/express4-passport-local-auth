const express = require('express');
const router = express.Router();
const middleware = require('./middleware.js');
const rawUrl = require('../lib/urlHelper').rawUrl;
const url = require('../lib/urlHelper').url;
const adminCtrl = require('../controllers/admin');
const csrf = require('../controllers/csrf');

/* Middleware AUTH,Roles */
router.use('/', middleware.isLoggedIns, middleware.needsRole(['manager','admin']));

/* Middleware to fetch user based on :userID*/
router.param('userId', middleware.userIdParamFetch);

/* GET Admin Index */
router.get('/', adminCtrl.renderAdminHome);

/* Create User */
router.get(rawUrl('admin.users.create'), csrf.csrfProtection, adminCtrl.createUserForm);
router.post(rawUrl('admin.users.create'), csrf.parseForm, csrf.csrfProtection, adminCtrl.createUser);

/* GET List Users redirect */
router.get('/users', ((req, res, next) => {res.redirect(url('admin.users'))}));

/* GET List Users */
router.get(rawUrl('admin.users'), middleware.isPageIntegerPositive, middleware.usersPagination, adminCtrl.renderUsersList);

/* GET Users Profile */
router.get(rawUrl('admin.users.profile'), csrf.csrfProtection, adminCtrl.renderUserProfile);

/* Patch Users */
router.patch(rawUrl('admin.users.profile'), csrf.parseForm, csrf.csrfProtection, adminCtrl.patchUser);

/* Delete User */
router.delete(rawUrl('admin.users.delete'), csrf.parseForm, csrf.csrfProtection, adminCtrl.deleteUser);

module.exports = router;
"use strict";

const url = require('../lib/urlHelper').url;
const models = require("../db/models");
const userCtrl = require('../controllers/user');
const roleCtrl = require('../controllers/roles');
const validator = require('validator');

/**
 * isLoggedIns
 *
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function isLoggedIns(req, res, next) {
    let is_json = false;
    if (req.accepts(['html', 'json']) === 'json') is_json = true;
    //todo expiration
    if (req.isAuthenticated()) {
        const expiration = new Date(req.user.password_expiration);
        const now = new Date();
        if (now >= expiration) {
            return res.redirect('/user/reset_password');
        }
        return next();
    } else {
        if (is_json) return res.status(401).json({redirect: url('auth.login')});
        return res.redirect(url('auth.login'));
    }
}

/**
 * isLoggedInsPasswordReset
 *
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function isLoggedInsPasswordReset(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    } else {
        res.redirect(url('home'));
    }
}

/**
 * isLoggedInOrLocalHost
 *
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function isLoggedInOrLocalHost(req, res, next) {
    if (req.isAuthenticated() || req.ip === '127.0.0.1' || req.ip === '::ffff:127.0.0.1') {
        return next();
    } else {
        console.log('IP not login : ' + req.ip);
        res.redirect(url('home'));
    }
}

/**
 * needs Role 
 * ['admin'] ['manager','admin']
 * 
 * @param role can be string or Array
 * @returns {Function}
 */
var needsRole = function (role) {
    return function (req, res, next) {
        let found = false;
        if(req.user && req.user.role()){
            found = roleCtrl.needsRole(req.user,role)
        }
        if(found) return next();
        return res.status(401).send('Unauthorized')
    };
};

/**
 * Users Pagination
 *
 * @param req
 * @param res
 * @param next
 */
function usersPagination(req, res, next) {
    const limit = 10;

    let page = 0;
    if (req.params.page) {
        page = parseInt(req.params.page);
    }
    page = (page - 1) > 0 ? page - 1 : 0;

    const offset = page * limit;

    models.User.findAndCountAll(
        {
            limit: parseInt(limit),
            offset: parseInt(offset),
            include: [{model: models.Role}]
        }
    ).then(users => {
        req.users = users;
        res.locals.total = users.count;
        res.locals.pages = Math.ceil(users.count / limit);
        res.locals.page = page;
        if (offset < users.count) res.locals.prev = true;
        if (offset > limit) res.locals.next = true;
        next();
    }, (e => {
            console.error('usersPagination ' + e);
            return res.status(500).end();
        }
    ));
}

/**
 * :page is integer and positive
 *
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function isPageIntegerPositive(req, res, next) {
    if (!req.params.page) return next();

    if (parseInt(req.params.page, 10) >= 0) {
        return next();
    }

    console.error('404 isPageIntegerPositive');
    return res.status(404).end();
}

/**
 * UserId param fetch
 *
 * @param req
 * @param res
 * @param next
 * @param userId
 * @returns {*}
 */
function userIdParamFetch(req, res, next, userId) {
    if (!validator.isInt(userId + '', {gt: 0}))
        return res.status(404).json({
            error: 'user NOT FOUND Fuck Id ' + userId
        });
    userCtrl.getUserById(userId).then(function (user) {
        if (!user) return res.status(404).end();
        res.locals.luser = user;
        next();
    }, function (e) {
        console.log('Error User_app' + JSON.stringify(e));
        return res.status(400).json(e)
    })
}

module.exports = {
    isLoggedIns: isLoggedIns,
    needsRole: needsRole,
    isLoggedInOrLocalHost: isLoggedInOrLocalHost,
    isLoggedInsPasswordReset: isLoggedInsPasswordReset,
    usersPagination: usersPagination,
    isPageIntegerPositive: isPageIntegerPositive,
    userIdParamFetch: userIdParamFetch
};
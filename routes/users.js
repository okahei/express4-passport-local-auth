var express = require('express');
var router = express.Router();
var middleware = require('./middleware.js');
var models = require("../db/models");
var _ = require('underscore');
var userCtrl = require('../controllers/user');
var colors = require('colors');
var validator = require('validator');
const url = require('../lib/urlHelper').rawUrl;

/* MIDDLEWARE AUTH*/
router.use('/', function (req, res, next) {
    middleware.isLoggedIns(req, res, next)
});

/**
 * fetch req.user_app ID
 * MIDDLEWARE
 */
router.param('id', function (req, res, next, id) {
    if (!validator.isInt(id + '', {gt: 0}))
        return res.status(404).json({
            error: 'user NOT FOUND Fuck Id'
        });
    userCtrl.getUserById(id).then(function (user) {
        if (!user) return res.status(404);
        req.user_app = user;
        next();
    }, function (e) {
        console.log('Error User_app' + JSON.stringify(e));
        return res.status(400).json(e)
    })
});

router.get('/new', function (req, res, next) {
    res.render('admin/new_user', {
        title: 'new User',
        user: req.user
    })
});

router.get(url('users.changepassword'), function (req, res, next) {
    return res.send(200).end();
});

router.get(url('users.profile'), function (req, res, next) {
    res.render('users/profile', {
        title: 'Profile',
        user: req.user
    })
});

/* GET users listing. */
/*router.get(
    url('users.list'),
    middleware.usersPagination,
    function (req, res, next) {
        res.render('admin/users', {
            title: 'Users list',
            user: req.user,
            users: req.users
        })
    }
);*/

/**
 * user_app GET ID
 */
router.get('/:id', function (req, res, next) {
    res.render('admin/edit_user', {
        title: 'Edit User',
        panelTitle: 'Editar User',
        user: req.user,
        user_app: req.user_app,
        roles: roles.roles
    })
});

/**
 * CREATE USER POST
 */
router.post('/', function (req, res, next) {
    console.log('User New: ' + JSON.stringify(req.body));
    var user = req.body;
    var usernamePromise = null;
    usernamePromise = models.User.findOne({
        where: {
            username: user.username
        }
    });

    return usernamePromise.then(function (model) {
        if (model) {
            return res.status(422).json('Usuario ya existe')
        } else {
            //****************************************************//
            // MORE VALIDATION GOES HERE(E.G. PASSWORD VALIDATION)
            //****************************************************//

            models.User.create({
                username: user.username,
                email: user.email,
                password: user.password,
                roleId: user.roleId,
            }).then(function (luser) {
                return res.status(200).json(luser)
            });
        }
    });
});

/**
 * USER PATCH
 */
router.patch('/', function (req, res, next) {
    console.log('User PATCH: ' + JSON.stringify(req.body));
    var body_user = req.body;
    var user = {};
    if (body_user.hasOwnProperty('password')) {
        var password = body_user.password;
        if (password.length >= 6) {
            user.password = password;
        } else {
            return res.status(500).json({error: "password menos 6 caracteres"})
        }
    }

    user.id = body_user.id;
    user.username = body_user.username;
    user.email = body_user.email;
    user.group = body_user.group;
    user.is_admin = body_user.is_admin;

    models.User.find({
        where: {
            id: user.id
        }
    }).then(function (luser) {
        luser.update(user).then(function (updated_user) {
            return res.status(200).json(updated_user)
        }, function (e) {
            return res.status(500).json({error: e})
        });
    }, function (e) {
        return res.status(500).json({error: e})
    });
});


module.exports = router;

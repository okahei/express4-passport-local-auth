/*
 * Mocha Test Express App Auth login Form
 *
 * AppReady Event:
 *   Before start testing Mocha will wait for 'AppReady' Event so
 *   we are sure that, for example, sequelize is connected.
 *
 * Csrf Token:
 *   Before POST request to login form we have to extract _CSRF field
 *   from a previous GET request.
 *   Cheerio allows to "load Jquery" to extract csrf form field
 *   https://medium.com/@IvanAli/retrieving-the-csrf-token-to-log-in-from-a-test-function-in-node-js-910daacbdc2d#.jybjlcqqs
 *
 * */


const app = require('../app'); // express app
const url = require('../lib/urlHelper').url;

const supertest = require('supertest');
const agent = supertest.agent(app);
const should = require('should');
const appconf = require(__dirname + '/../conf/app.json');
const authconf = require(__dirname + '/../conf/auth.json');
const firstAdmin = appconf.users.firstAdmin;
const sessionRegex = new RegExp(authconf.session.name);
const extractCsrfToken = require('./lib/testLib');
let csrf, Cookies;


/**
 * Login First Admin User
 *
 */
describe('Login First Admin User', function () {
    it('should login correctly', function (done) {
        agent
            .get(url('auth.login'))
            .end(function (err, res) {
                csrf = extractCsrfToken(res);
                agent
                    .post(url('auth.login'))
                    .type('form')
                    .send({username: firstAdmin.username, password: firstAdmin.password, _csrf: csrf})
                    .expect(302)
                    .expect('Location', url('home'))
                    .expect('set-cookie', sessionRegex)
                    .end(function (err, res) {
                        //console.log(res.headers)
                        if (err) return done(err);
                        Cookies = res.headers['set-cookie'];
                        return done();
                    });
            });
    });
});

/**
 * Use Cookies
 *
 */
describe('Login with Cookies', function () {
    it('should login correctly', function (done) {
        agent
            .get(url('home'))
            .set('Cookie', Cookies)
            .expect('set-cookie',sessionRegex)
            .end(function (err, res) {
                //console.log(Cookies)
                if (err) return done(err);
                res.text.should.containEql('<h2>Hi superman</h2>');
                done();
            })
    });
});
/**
 * Incorrect Auth Attempt
 */
describe('Incorrect Auth Attempt: ', function () {
    it('should Fail login', function (done) {
        agent
            .get(url('auth.login'))
            .end(function (err, res) {
                csrf = extractCsrfToken(res);
                agent
                    .post(url('auth.login'))
                    .type('form')
                    .send({username: "f", password: "bar", _csrf: csrf})
                    .expect(200)
                    .end(function (err, res) {
                        if (err) return done(err);
                        res.text.should.containEql(url('auth.forgot'));
                        res.text.should.containEql('Incorrect username');
                        return done();
                    });
            });
    });
});


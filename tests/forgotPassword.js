/*
 * Mocha Test Express App Auth login Form
 *
 * AppReady Event:
 *   Before start testing Mocha will wait for 'AppReady' Event so
 *   we are sure that, for example, sequelize is connected.
 *
 * Csrf Token:
 *   Before POST request to login form we have to extract _CSRF field
 *   from a previous GET request.
 *   Cheerio allows to "load Jquery" to extract csrf form field
 *   https://medium.com/@IvanAli/retrieving-the-csrf-token-to-log-in-from-a-test-function-in-node-js-910daacbdc2d#.jybjlcqqs
 *
 * */

const app = require('../app'); // express app
const url = require('../lib/urlHelper').url;

const supertest = require('supertest');
const agent = supertest.agent(app);
const should = require('should');
const appconf = require(__dirname + '/../conf/app.json');
const authconf = require(__dirname + '/../conf/auth.json');
const firstAdmin = appconf.users.firstAdmin;
const sessionRegex = new RegExp(authconf.session.name);
const extractCsrfToken = require('./lib/testLib');
let csrf, Cookies, reset_token;


/**
 * Login First Admin User
 *
 */
describe('Forgot Password', function () {
    it('should send token email', function (done) {
        agent
            .get(url('auth.forgot'))
            .end(function (err, res) {
                csrf = extractCsrfToken(res);
                agent
                    .post(url('auth.forgot'))
                    .type('form')
                    .send({email: firstAdmin.email, _csrf: csrf})
                    .expect(302)
                    .expect('set-cookie', sessionRegex)
                    .end(function (err, res) {
                        if (err) return done(err);
                        Cookies = res.headers['set-cookie'].pop().split(';')[0];
                        agent
                            .get(url('auth.forgot'))
                            .set('Cookie', Cookies)
                            .end(function (err, res) {
                                if (err) return done(err);
                                res.text.should.containEql(firstAdmin.email);
                                return done();
                            })
                    });
            });
    });


    it('should get token email from Maildev', function (done) {
        const host = "http://localhost:" + appconf.development.maildev.web;
        supertest(host)
            .get('/email')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function (err, res) {
                if (err) return done(err);
                const email = JSON.parse(res.text)[0];
                let reset_url_match = email.text.match(/(https?:\/\/[^\s]+)/i);
                if (!reset_url_match) return done(new Error('no match url token in email body text'));

                if (Array.isArray(reset_url_match)) {
                    //found url for reset password in email body
                    reset_token = reset_url_match[0].split('/').reverse()[0];
                    return done();
                }
                return done(new Error('token not found'))
            });
    });

    it('should get reset form and change password successfully', function (done) {
        agent
            .get(url('auth.reset',{token: reset_token}))
            .expect(200)
            .end(function (err, res) {
                if (err) return done(err);
                csrf = extractCsrfToken(res);
                agent
                    .post(url('auth.reset',{token: reset_token}))
                    .type('form')
                    .send({password: firstAdmin.password, password1: firstAdmin.password, _csrf: csrf})
                    .expect(302)
                    .expect('Location', url('home'))
                    .expect('set-cookie', sessionRegex)
                    .end(function (err, res) {
                        if (err) return done(err);
                        return done();
                    });
            })
    });

    it('should Fail to reset a users password', function (done) {
        agent
            .get(url('auth.reset',{token: "729c256257ffcae6da3396611e248ffdd98603b5"}))
            .expect(302)
            .expect('Location', url('auth.forgot'))
            .end(function (err, res) {
                if (err) return done(err);
                csrf = extractCsrfToken(res);
                agent
                    .post(url('auth.reset',{token: "729c256257ffcae6da3396611e248ffdd98603b5"}))
                    .type('form')
                    .send({password: firstAdmin.password, password1: firstAdmin.password, _csrf: csrf})
                    .expect(403)
                    .end(function (err, res) {
                        if (err) return done(err);
                        return done();
                    });
            })
    })
});

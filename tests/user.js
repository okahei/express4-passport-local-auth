/*
 * Mocha Test Express App Auth login Form
 *
 * AppReady Event:
 *   Before start testing Mocha will wait for 'AppReady' Event so
 *   we are sure that, for example, sequelize is connected.
 *
 * Csrf Token:
 *   Before POST request to login form we have to extract _CSRF field
 *   from a previous GET request.
 *   Cheerio allows to "load Jquery" to extract csrf form field
 *   https://medium.com/@IvanAli/retrieving-the-csrf-token-to-log-in-from-a-test-function-in-node-js-910daacbdc2d#.jybjlcqqs
 *
 * */


const app = require('../app'); // express app
const url = require('../lib/urlHelper').url;

const supertest = require('supertest');
const agent = supertest.agent(app);
const should = require('should');
const appconf = require(__dirname + '/../conf/app.json');
const authconf = require(__dirname + '/../conf/auth.json');
const firstAdmin = appconf.users.firstAdmin;
const sessionRegex = new RegExp(authconf.session.name);
const extractCsrfToken = require('./lib/testLib');
let csrf, Cookies;


/**
 * Login First Admin User
 *
 */
describe('Login Admin User in order to update luser profile', function () {
    it('should login correctly', function (done) {
        agent
            .get(url('auth.login'))
            .end(function (err, res) {
                Cookies = res.headers['set-cookie'];
                csrf = extractCsrfToken(res);
                agent
                    .post(url('auth.login'))
                    .set('cookie', Cookies)
                    .type('form')
                    .send({username: firstAdmin.username, password: firstAdmin.password, _csrf: csrf})
                    .expect(302)
                    .expect('Location', url('home'))
                    .expect('set-cookie', sessionRegex)
                    .end(function (err, res) {
                        //console.log(res)
                        if (err) return done(err);
                        Cookies = res.headers['set-cookie'];
                        //res.text.should.containEql("Hi superman");
                        return done();
                    });
            });
    });
});

/**
 * Update User
 */
describe('Update User', function () {
    const luserUrl = url('admin.users.profile',{userId:10});

    it('should update User', function (done) {
        agent
            .get(luserUrl)
            .set('cookie', Cookies)
            .expect(200)
            .end(function (err, res) {
                if(err) return done(err);
                res.text.should.containEql("Profile");
                csrf = extractCsrfToken(res);
                Cookies = res.headers['set-cookie'].map(co => co.split(';')[0]).toString().replace(',','; ');
                agent
                    .post(luserUrl)
                    .set('cookie', Cookies)
                    .type('form')
                    .send({
                        username: "PepitoFooBarr",
                        password:"",
                        email:"pepitoFooBar1@super.local",
                        roleId:"1",
                        _method: "patch",
                        _csrf: csrf
                    })
                    .expect(302)
                    .expect('Location', luserUrl)
                    .end(function (err, res) {
                        if (err){
                            //console.log(res.headers)
                            return done(err);
                        }
                        return done();
                    });
            });
    });
});


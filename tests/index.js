const app = require('../app'); // express app
const appconf = require(__dirname + '/../conf/app.json');
const eventReadyMsg = appconf.events.ready.msg;
const loginTest = require('./login');
const forgotPasswordTest = require('./forgotPassword');
const userTest = require('./user');

/**
 * We need to wait for AppReady EVENT
 * Emitted from App.js
 */
before(function (done) {
    app.on(eventReadyMsg, function () {
        app.ready = true;
        done();
    });
});
const cheerio = require('cheerio');

/**
 * GET CSRF TOKEN FORM
 *
 * @param res (response html)
 * @returns CSRF TOKEN STRING
 */
function extractCsrfToken(res) {
    let $ = cheerio.load(res.text);
    return $('[name=_csrf]').val();
}

module.exports = extractCsrfToken;
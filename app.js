/* if root start app, downgrade to nobody nogroup*/
const setAppUidGid = require('./conf/setAppUidGid');

/* require App appconf */
const appconf = require('./conf/app.json');

/* NODE_ENV = development?  */
process.env.NODE_ENV = appconf.mode;
process.env.BLUEBIRD_WARNINGS = 0;

/* App PORT 3000? */
process.env.PORT = appconf.port;

/* common requires */
const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const colors = require('colors/safe');
const _ = require('underscore');

/* Sessions */
const session = require('express-session');
const RedisStore = require('connect-redis')(session);

/* Flash msg */
const flash = require('express-flash');

/* tamper headers */
const helmet = require('helmet');

/* methodOverride frm _method=[patch,delete] */
const methodOverride = require('method-override');


/* setup Routes */
const routes = require('./conf/routes');
routes.home.controller = require('./routes/home');
routes.auth.controller = require('./routes/auth');
//routes.users.controller =  require('./routes/users');
routes.admin.controller = require('./routes/admin');

/* define our new express App */
const app = express();

/*
 *  Setup App.locals
 */

/* export routes */
app.locals.routes = routes;
app.locals.url = require('./lib/urlHelper').url;

/* Make underscore available under view templates */
app.locals._ = _;

/* ASSETS: css/js assets available under view templates */
app.locals._assets = require('./public/config/assets.config.json');

/* export appconf */
app.locals._appconf = appconf;

/* tpl helper to needRole func */
app.locals.needsRole = require('./controllers/roles').needsRole;

/*
 *  view engine setup 
 */
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

/* logger dev, if mocha testing suppress all express console.log */
if (process.env.MOCHA_TEST != 1) {
    app.use(logger('dev'));
}

/* bodyParsers */
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

/* MethodOverride Middleware */
app.use(methodOverride(function (req, res) {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method;
        delete req.body._method;
        return method;
    }
}));

/* sessions config */
const authConfig = require("./conf/auth.json");

/* cookieParser with same secret as sessions for signed cookies */
app.use(cookieParser(authConfig.session.secret));

/* Redis to store sessions */
authConfig.session.store = new RedisStore(authConfig.redis);

/* setup Sessions */
app.use(session(authConfig.session));

/* Serve Static Content */
app.use(express.static(path.join(__dirname, 'public')));

/* favicon.ico */
app.use(favicon(path.join(__dirname, 'public/images', 'favicon.ico')));

/* Passport Auth System */
const _passport_auth = require('./auth/passport/Auth').auth(app);

/* Flash Messages */
app.use(flash());

/* if there's a flash message in the session request,
 make it available in the response, then delete it */
app.use(function (req, res, next) {
    res.locals.flash = req.session.flash;
    delete req.session.flash;
    next();
});

/* HELMET fake headers */
app.use(helmet.hidePoweredBy({setTo: 'PHP 4.2.0'}));

/* Routes mount points */
app.use(routes.home.mount, routes.home.controller);
app.use(routes.auth.mount, routes.auth.controller);
//app.use(routes.users.mount, routes.users.controller);
app.use(routes.admin.mount, routes.admin.controller);


// handle csrf errors specifically
app.use(function (err, req, res, next) {
    if (err.code !== 'EBADCSRFTOKEN') return next(err);
    res.status(403).json({"error": "session has expired or tampered with weee"});
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

/*
 * Events to listen before emit appReady Event
 *  Events must be defined at config/app.json
 *      For example, sequelize sync event
 */
const isAppReady = new (require('./controllers/isAppReady'))({
    events: appconf[process.env.NODE_ENV].events,
    emitter: app,
    msg: appconf.events.ready.msg
});

/*listen for AppReady Event*/
app.ready = false;
app.on(appconf.events.ready.msg, function () {
    app.ready = true;
    if (process.env.MOCHA_TEST == 1) return;
    console.log(colors.white('App is Ready on port ' + process.env.PORT));
    console.log(" ");
    console.log(colors.grey(colors.underline('ROUTE List')));
    const route_list = require('./lib/route_list')(routes);
    console.log(route_list);
});

/*
 *  Sequelize sync with DB
 *      Create New Admin if no one is Admin
 *
 */
const models = require('./db/models');
const userCtrl = require('./controllers/user');
const roleCtrl = require('./controllers/roles');
const create_admin = require('./db/faker/createAdminUser');

models.sequelize
    .sync()
    .then(() => {

        roleCtrl
            .getAll()
            .then(roles => {
                if (roles.length == 0) { //no Roles? seed table roles...
                    console.log(colors.blue('populating Roles Table'));
                    const userRoles = Object.keys(appconf.users.roles)
                        .map(role => {
                            return {role: appconf.users.roles[role]}
                        });
                    return models.Role.bulkCreate(userRoles);
                }
            })
            .then(() => {
                userCtrl
                    .findOneAdmin()
                    .then(user => {
                        if (!user) return create_admin(appconf.users.firstAdmin);
                    })
                    .then(() => isAppReady.iAmReady('sequelize'))
            })

    }).catch(e => {
        console.error(e);
        process.exit();
        throw e;
    });

/**
 * Maildev Fake Smtpd Server
 *  started if we are in dev mode
 */
if (process.env.NODE_ENV === "development") {
    const maildev = require('./maildev/smtpd');

    maildev.start(function (err) {
        if (err) throw new Error(err);
        isAppReady.iAmReady('maildev');
    });
}


module.exports = app;

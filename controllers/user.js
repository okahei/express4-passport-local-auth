"use strict";
/* require App appconf */
const appconf = require('../conf/app.json');
const models = require('../db/models');

/**
 * getUserById
 *
 * @param id
 * @returns {Promise}
 */
function getUserById(id) {
    return models.User.findOne({
        where: {
            id: id
        },
        attributes: ['id', 'username', 'email', 'password_expiration'],
        include: [{
            model: models.Role
        }]
    })
}

/**
 * getUserByName
 *
 * @param username
 * @returns {Promise}
 */
function getUserByName(username) {
    return models.User.findOne({
        where: {
            username: username
        },
        include: [{
            model: models.Role
        }]
    })
}

/**
 * fina all users with their roles
 *
 * @returns Promise
 */
function findAndCountAll(params) {
    if (!params) throw new Error('User Controler findAndCountAll params missing');

    params.limit = params.limit || 20;
    params.offset = params.offset || 0;

    console.log('Limit ' + params.limit + ' Offset: ' + params.offset);
    return models.User.findAndCountAll(
        {
            limit: parseInt(params.limit),
            offset: parseInt(params.offset),
            include: [{model: models.Role}]
        }
    ).then(userlist => userlist)
}

/**
 * Create user
 *
 * @param user
 * @returns {*|{method}|Promise.<this|Errors.ValidationError>}
 */
function create(user) {
    return models.User.create(user);
}

/**
 * Destroy User
 *
 * @param id
 * @returns {Promise}
 */
function destroy(id) {
    return new Promise(function (resolve, reject) {
        models.User.findOne({where: {id: parseInt(id)}}).then(function (user) {
            if (!user) return reject(new Error('destroy userCtrl, No user Id found'));
            user.destroy().then((() => resolve()))
        })
    })
}

/**
 * Find Duplicate username
 *
 * @param id
 * @param username
 * @returns {*}
 */
function findDuplicateUserName(id, username) {
    return new Promise((resolve, reject) => {
        models.User.findOne({
            where: {
                id: {
                    $not: id
                },
                username: username
            }
        }).then(user => {
            return resolve(user);
        }).catch(e => reject(e))
    })
}

/**
 * Find if one user is Admin
 * 
 * @returns {Promise}
 */
function findOneAdmin() {
    return new Promise((resolve, reject) => {
       models.User.findOne({
            include: [{
                model: models.Role,
                where: {
                    role: appconf.users.roles.admin
                }
            }]
        })
            .then(user => {
                return resolve(user)
            })
            .catch(e => {throw new(e)})
    })
}


module.exports = {
    getUserById: getUserById,
    getUserByName: getUserByName,
    findAndCountAll: findAndCountAll,
    create: create,
    destroy: destroy,
    isDupName: findDuplicateUserName,
    findOneAdmin:findOneAdmin
};
"use strict";

const userCtrl = require('./user');
const roleCtrl = require('./roles');
const models = require('../db/models');
const url = require('../lib/urlHelper').url;

/**
 * Form render user create
 *
 * @param req
 * @param res
 * @param next
 */
function createUserForm(req, res, next) {
    roleCtrl.getRolesNames().then(function (roles) {
        res.render('admin/users/create', {
            title: 'Create new User',
            user: req.user,
            roles: roles,
            csrfToken: req.csrfToken()
        });
    })
}
/**
 * Create User
 *
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function createUser(req, res, next) {
    userCtrl.create(req.body).then(function (user) {
        return res.redirect(url('admin.users.profile', {userId: user.id}));
    }, function (e) {
        next(e)
    })
}

/**
 * Delete USer
 *
 * @param req
 * @param res
 * @param next
 */
function deleteUser(req, res, next) {
    //todo validate id
    userCtrl.destroy(req.body.id).then(function () {
        req.flash('info', 'User Deleted');
        return res.redirect(url('admin.users'))
    }, function (e) {
        return res.status(500).end();
    });
}

/**
 * Patch User
 *
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function patchUser(req, res, next) {
    const id = res.locals.luser.id;
    const username = req.body.username;
    const roleId = req.body.roleId;

    /* no password update */
    /* http://stackoverflow.com/questions/16585209/node-js-object-object-has-no-method-hasownproperty */
    
    if(Object.prototype.hasOwnProperty.call(req.body, 'password'))
    {
        if(req.body.password.length === 0)
        {
            delete req.body.password;
            
        } else {
            if (req.body.password !== req.body.password_repeat)
            {
                console.log("Error passwords do not match")
                req.flash("error", "Error passwords do not match");
                return res.redirect('back')
                
            } else if(req.body.password.length <= 3) {
                console.log("Error passwords <= 3")
                req.flash("error", "Error password min 4 chars");
                return res.redirect('back')
            }
        }
    }

    /* find if user is choosing a username that is already taken */
    userCtrl.isDupName(id, username)
        .then(user => {
            if (user) {
                req.flash('error', 'Username Taken');
                return res.redirect('back');
            }
            roleCtrl.getById(roleId)
                .then(role => {
                    if (!role) return res.status(404).end();
                    /* update user */
                    res.locals.luser.update(req.body)
                        .then(() => {
                            req.flash('success', 'Profile updated');
                            return res.status(200).redirect(url('admin.users.profile', {userId: id}))
                        }).catch(e => res.status(500).end())
                }).catch(e => res.status(500).end())
        }).catch(e => res.status(500).end());
}

/**
 * Render Admin Home
 *
 * @param req
 * @param res
 * @param next
 */
function renderAdminHome(req, res, next) {
    res.render('admin/index', {
        title: 'Admin',
        user: req.user,
        users: req.users
    });
}

/**
 * Render Users collection
 *
 * @param req
 * @param res
 * @param next
 */
function renderUsersList(req, res, next) {
    res.render('admin/users/list', {
        title: 'Users',
        user: req.user,
        users: req.users
    });
}

/**
 * Render Users Profile
 *
 * @param req
 * @param res
 * @param next
 */
function renderUserProfile(req, res, next) {
    roleCtrl.getRolesNames().then((roles => {
        res.render('admin/users/profile', {
            title: 'Users',
            user: req.user,
            roles: roles,
            msg: res.locals.flash,
            csrfToken: req.csrfToken(),
            action: url('admin.users.profile', {userId: res.locals.luser.id})
        });
    }))
}


module.exports = {
    renderUserProfile: renderUserProfile,
    renderUsersList: renderUsersList,
    renderAdminHome: renderAdminHome,
    patchUser: patchUser,
    deleteUser: deleteUser,
    createUser: createUser,
    createUserForm: createUserForm
};
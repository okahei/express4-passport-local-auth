"use strict";

const models = require('../db/models');

/**
 * Get Roles table
 *
 * @returns {*}
 */
function getRolesNames() {
    return models.Role.findAll()
        .then(roles => roles.map(r => r.toJSON()))
}

/**
 * get by Id
 *
 * @param id
 * @returns {Promise}
 */
function getById(id) {
    return new Promise((resolve, reject) => {
        models.Role.findOne({
            where: {
                id: parseInt(id)
            }
        }).then(role => resolve(role)).catch(e => reject(e))
    })
}

/**
 * Get all Roles
 *
 * @returns {*}
 */
function getAll() {
    return models.Role.findAll()
}

/**
 * Needs Role
 * 
 * @param user
 * @param role
 * @returns {*}
 */
function needsRole(user, role) {
    return roleToArray(role)
        .find(role => {
            return user.role() === role;
        });
}

/**
 * Role string to array
 *
 * ('admin') => ['admin']
 * (['manager', 'admin']) => ['manager', 'admin']
 *
 * @param role
 * @returns Array of Roles
 */
function roleToArray(role) {
    if (Object.prototype.toString.call(role) === "[object String]")
        return [role];
    return role;
}

module.exports = {
    getRolesNames: getRolesNames,
    getById: getById,
    getAll: getAll,
    needsRole:needsRole
};
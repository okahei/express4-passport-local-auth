/* app config */
const appConf = require(__dirname + '/../conf/app.json');
const sendEmail = require(__dirname + '/nodemailer');
const url = require('../lib/urlHelper').url;

/**
 * Change Password mail confirm
 *
 * @param user
 * @returns {Promise}
 */
function changePasswordMailer(user) {
    return sendEmail({
        to: user.email,
        from: 'passwordreset@demo.com',
        subject: 'Your password has been changed',
        text: 'Hello,\n\n' +
        'This is a confirmation that the password for your account ' + user.email +
        ' has just been changed.\n'
    });
}

function forgotPasswordMailer(user) {
    const user_reset_url = url('auth.reset',{token: user.resetPasswordToken });
    return sendEmail({
        to: user.email,
        from: 'passwordreset@demo.com',
        subject: 'Node.js Password Reset',
        text: 'You are receiving this because you (or someone else) have requested the reset of the password' +
        ' for your account.\n\n Please click on the following link, or paste this into your browser ' +
        'to complete the process:\n\n' +
        'http://' + appConf.host + ':' + appConf.port  + user_reset_url + '\n\n' +
        'If you did not request this, please ignore this email and your password will remain unchanged.\n'
    })
}

module.exports = {
    changePasswordMailer: changePasswordMailer,
    forgotPasswordMailer: forgotPasswordMailer
};
"use strict";

/**
 * Create server http|https based on app mode
 *
 * @param app
 * @return server [http|https]
 */
function createServer(app) {
    console.log(process.env.NODE_ENV);
    if (process.env.NODE_ENV === 'production') {
        const https = require('https');
        const options = require('../conf/ssl');

        return https.createServer({
            key: options.key,
            cert: options.cert
        }, app);

    } else {
        const http = require('http');
        return http.createServer(app);
    }
}

module.exports = createServer;
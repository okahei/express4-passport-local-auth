"use strict";

const models = require('../db/models');
const log = require('./log');
const passport = require('passport');
const async = require('async');
const validator = require('validator');
const mailer = require('../controllers/mailer');
const crypt = require('crypto');
const url = require('../lib/urlHelper').url;
const debug = require('debug')('auth');
const colors = require('colors/safe');
/**
 * Render Login Page
 *
 * @param req
 * @param res
 * @param error
 */
function renderLogin(req, res, error) {
    return res.render('login', {
        title: 'Init Session',
        error: error,
        csrfToken: req.csrfToken()
    });
}

/**
 * Get Login Page
 *
 * If user is authenticated redirect to root
 *
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function loginGet(req, res, next){
    debug('Get login page');
    if (req.isAuthenticated()) return res.redirect(url('home'));
    return renderLogin(req, res);
}

/**
 * Render Forgot password Page
 *
 * @param req
 * @param res
 * @param next
 */
function forgotGet(req, res, next){
    res.render('users/forgot.ejs', {
        title: "Recover Password",
        msg: res.locals.flash,
        csrfToken: req.csrfToken()
    });
}

/**
 * Post Forgot password Page
 *
 * Should generate a token and send it to the user by email
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function forgotPost(req, res, next) {
    if (validator.isEmail(req.body.email) == false) {
        req.flash('error', 'Not valid Email.');
        return res.redirect(url('auth.forgot'));
    }
    async.waterfall([
        function (done) {
            crypt.randomBytes(20, function (err, buf) {
                var token = buf.toString('hex');
                done(err, token);
            });
        },
        function (token, done) {
            models.User.findOne({
                where: {
                    email: req.body.email
                }
            }).then(function (user) {
                if (!user) {
                    req.flash('error', 'No email found');
                    return res.redirect(url('auth.forgot'));
                }
                user.resetPasswordToken = token;
                user.resetPasswordExpires = Date.now() + 3600000; // 1 hour
                user.save().then(function (luser) {
                    done(null, luser);
                });
            })
        },
        function (user, done) {
            mailer
                .forgotPasswordMailer(user)
                .then(function (info) {
                    req.flash('info', 'Recovery Email Sent to: ' + user.email);
                    done(null, 'done');
                }, function (e) {
                    done(e, 'done');
                });
        }
    ], function (err) {
        if (err) return next(err);
        res.redirect(url('auth.forgot'));
    });
}

/**
 * Passport Auth
 *
 * Should authenticate a user and log it
 * @param req
 * @param res
 * @param next
 */
function passportAuth(req, res, next) {
    debug('Post Login Page');
    passport.authenticate('local', {failureFlash: true}, function (err, user, info) {
        if (err) return next(err);

        if (!user) return renderLogin(req, res, info);

        req.logIn(user, function (err) {
            if (err) return next(err);
            log.info('User Auth: ' + req.user.username + ', IP: ' + req.ip);
            return res.redirect(url('home'));
        });
    })(req, res, next);
}

/**
 * Get Reset page
 *
 * should render change password form
 * @param req
 * @param res
 * @param next
 */
function resetGet(req, res, next) {
    models.User.findOne({
        where: {
            resetPasswordToken: req.params.token,
            resetPasswordExpires: {$gt: Date.now()}
        }
    }).then(function (user) {
        if (!user) {
            req.flash('error', 'Password reset token invalid');
            return res.redirect(url('auth.forgot'));
        }
        res.render('users/reset', {
            title: 'Reset Password',
            csrfToken: req.csrfToken(),
            msg: res.locals.flash,
            resetPasswordToken: req.params.token
        });
    }, function (e) {
        console.error('Find Token error');
        console.error(e);
        req.flash('error', 'Find Token error');
        return res.redirect(url('auth.forgot'));
    });
}

/**
 * Post Reset page
 *
 * Should change users password,
 *  mail the event to the user and redirect him to root
 * @param req
 * @param res
 * @param next
 */
function resetPost(req, res, next) {
    var new_password = req.body.password;
    var token = req.params.token;
    /*todo verify passwords same*/
    async.waterfall([
        function (done) {
            models.User.findOne({
                where:{
                    resetPasswordToken: token,
                    resetPasswordExpires: {$gt: Date.now()}
                }
            }).then(function (user) {
                if (!user) {
                    req.flash('error', 'Reset token invalid');
                    return res.redirect('back');
                }

                user.password = new_password;
                user.resetPasswordToken = undefined;
                user.resetPasswordExpires = undefined;

                user.save().then(function (luser) {
                    req.logIn(luser, function (err) {
                        done(err, luser);
                    });
                });
            });
        },
        function (user, done) {
            mailer
                .changePasswordMailer(user)
                .then(function (info) {
                    //req.flash('success', 'your password has been successfully changed');
                    done(null, 'done');
                }, function (e) {
                    done(e, 'done');
                })
        }
    ], function (err) {
        if(err){
            console.error('ERROR Reset change Password');
            console.error(err);
            res.redirect('back');
        }

        /* change password ok redirect to home */
        res.redirect(url('home'));
    });
}

module.exports = {
    loginGet: loginGet,
    renderLogin: renderLogin,
    passportAuth:passportAuth,
    forgotGet:forgotGet,
    forgotPost:forgotPost,
    resetGet:resetGet,
    resetPost:resetPost
};
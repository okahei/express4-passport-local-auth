/* used to test it at command line */
process.env.NODE_ENV = process.env.NODE_ENV || "development";

/* app config */
var appConf = require(__dirname + '/../conf/app.json');
var config = appConf[process.env.NODE_ENV];

/* Error: self signed certificate from smtpd */
process.env.NODE_TLS_REJECT_UNAUTHORIZED = config.mailer.NODE_TLS_REJECT_UNAUTHORIZED;

var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');

/* create connection to smtpd server */
var transport = nodemailer.createTransport(smtpTransport(config.mailer.transport));

/**
 * Send Email
 *
 * @param email
 * @returns {Promise}
 */
function sendEmail(email) {
    return new Promise(function (resolve, reject) {
        transport.sendMail(email, function (err, info) {
            if (err) return reject(err);
            return resolve(info);
        });
    })
}

module.exports = sendEmail;
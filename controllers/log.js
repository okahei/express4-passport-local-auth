var bunyan = require('bunyan');
var appConf = require('../conf/app.json');

var logInfo = bunyan.createLogger({
    name: appConf.appName,
    streams: [{
        type: 'rotating-file',
        period: '1d', // daily rotation
        count: 3, // keep 3 back copies
        level: 'info',
        path: './logs/info.log'
    }]
});

var logError = bunyan.createLogger({
    name: appConf.appName,
    streams: [{
        type: 'rotating-file',
        period: '1d', // daily rotation
        count: 3, // keep 3 back copies
        level: 'error',
        path: './logs/error.log'
    }]
});

var logWarn = bunyan.createLogger({
    name: appConf.appName,
    streams: [{
        type: 'rotating-file',
        period: '1d', // daily rotation
        count: 3, // keep 3 back copies
        level: 'warn',
        path: './logs/warn.log'
    }]
});

var logDebugFile = bunyan.createLogger({
    name: appConf.appName,
    streams: [{
        type: 'rotating-file',
        period: '1d', // daily rotation
        count: 1, // keep 3 back copies
        level: 'debug',
        path: './logs/debug.log'
    }]
});

exports.error = function(msg) {
    logError.error(msg)
};

exports.info = function(msg) {
    logInfo.info(msg)
};

exports.warn = function(msg, payload) {
    logWarn.warn({
        "alert": msg
    }, payload)
};

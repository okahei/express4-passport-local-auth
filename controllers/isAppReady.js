"use strict";

/**
 * Class Emit App Ready Event
 */
class EmitAppReady {
    constructor(options)
    {
        /* options.emitter */
        if(typeof(options) != "object")
            throw new Error('EmitAppReady options not an Object');
        if(!options.hasOwnProperty('emitter'))
            throw new Error('EmitAppReady options Emitter missing');
        if(typeof(options.emitter.emit) != "function")
            throw new Error('EmitAppReady options Emitter.emit not a function');

        /* options.events */
        if(!options.hasOwnProperty('events'))
            throw new Error('EmitAppReady options Events missing');
        if(!Array.isArray(options.events))
            throw new Error('EmitAppReady options events not an Array');

        /* options.msg */
        if(!options.hasOwnProperty('msg'))
            throw new Error('EmitAppReady options msg missing');
        if(typeof(options.msg) != "string")
            throw new Error('EmitAppReady options msg not a String');

        /* properties */
        this.events = options.events;
        this.emitter = options.emitter;
        this.msg = options.msg;
    }

    /**
     * IAmReady:
     * 
     *  for each event received
     *  remove it from events array until its empty
     *  then emit this.msg
     * 
     * @param event
     * @returns {boolean}
     */
    iAmReady (event) {
        let index = this.events.indexOf(event);

        if (index > -1) { //found index
            this.events.splice(index, 1); //remove event from events
        }

        if (this.events.length === 0) { //no more events left
            this.emitter.emit(this.msg);
            return true;
        }

        return false;
    }
}

module.exports = EmitAppReady;
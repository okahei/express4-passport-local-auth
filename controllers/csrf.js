const csrf = require('csurf');
const bodyParser = require('body-parser');
const parseJson = bodyParser.json();
const parseUrlencoded = bodyParser.urlencoded({ extended: true });

module.exports = {
    csrfProtection: csrf({cookie: true}),
    parseForm: [parseJson, parseUrlencoded]
};
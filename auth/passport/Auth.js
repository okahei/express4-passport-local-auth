/**
 * Auth
 *
 * curl tip login
 * $ CSRF=`curl --cookie-jar ./cookies 'http://oka.local:3000/auth/login' | grep -A1 _csrf |tr -d ' '| awk -F'=' 'NR==2  {print $2}'|tr -d "\"|\>"`
 * $ curl --cookie-jar ./cookies -X POST -H
 *      'Content-Type: application/json'
 *      -d '{"username":"superman","password":"1234","_csrf":"'$CSRF'"}'
 *      -b ./cookies 'http://oka.local:3000/auth/login'
 *
 * @param app
 */
exports.auth = function (app) {
    var passport = require('passport'); //passport
    var LocalStrategy = require('passport-local').Strategy; //passport
    var bcrypt = require('bcrypt-nodejs');
    var userCtrl = require("../../controllers/user"); //passport
    //var limiter = require('express-limiter')(app, redisStore);
    
    // passport
    passport.use(new LocalStrategy(function (username, password, done) {
        userCtrl.getUserByName(username).then(function (user) {
            if (!user)
                return done(null, false, {message: 'Incorrect username.'});

            if (!bcrypt.compareSync(password, user.password))
                return done(null, false, {
                    message: 'Invalid username or password'
                });

            return done(null, user.toJSON());

        }, function (e) {
            return done(e);
        });
    }));

    app.use(passport.initialize()); //passport
    app.use(passport.session()); //passport

    passport.serializeUser(function (user, done) {
        done(null, user.id);
    });

    passport.deserializeUser(function (id, done) {
        userCtrl
            .getUserById(id)
            .then(function (user) {
                done(null, user);
            });
    });

};